<?php

namespace Database\Seeders;

use App\Models\Food;
use Faker\Factory;
use Illuminate\Database\Seeder;

class foodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++)
        {
            Food::create([
                'description' => $faker -> sentence(),
                'image' => $faker ->image('storage/images', $width = 640, $height = 480, null, false, 'food'), 
                'created_at'=> $faker -> dateTime($max = 'now', $timezone = null) 
            ]);
            

        }
    }
}
