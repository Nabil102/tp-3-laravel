<div class="container">
  <nav class="navbar navbar-expand-lg navbar-light bg-primary" d-flex">
    <div class="container-fluid">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <div class="p-4 flex-grow-1" ><a class="navbar-brand" href="{{route('home')}}">LOGO</a></div>
        <div>
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item p-4">
              <a class="nav-link active" aria-current="page" href="{{route('home')}}"><i class="fas fa-home"></i> Accueil</a>
            </li>
            <li class="nav-item p-4">
              <a class="nav-link" aria-current="page" href="#">À propos</a>
            </li>
            <li class="nav-item p-4">
                <a class="nav-link" href="{{ route('register') }}">Inscription</a>
            </li>
            <li class="nav-item p-4">
              @if (Auth::user())
                @if(Auth::user() -> role === 'MEMBER')
                <li class="nav-item">
                  <a class="nav-link active" href="{{ route('profil.index') }}">Profil</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" href="{{ route('food.index') }}">Ajouter Denrée</a>
                </li>
                @endif  
                <form method="POST" action="{{route('logout')}}">
                @csrf
                  <button type="submit" class="btn">Déconnexion</button>
                </form>
              @else
              
              <a class="nav-link" href="{{route('login')}}"  >Connexion</a>
              @endif
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
</div>
