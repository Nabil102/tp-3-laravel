@if($paginator -> hasPages() )
<ul class="pagination">
    @if ($paginator->onFirstPage())
    <li class="page-item disabled"><span class="page-link"> <i class="fas fa-backward"></i> </span> </li>
    @else 
    <li class="page-item"><a class="page-link" href="{{$paginator-> previousPageUrl()}}" rel="prev"><i class="fas fa-backward"></i></li>  
    @endif

    @foreach ($elements as $element)
        @if (is_string($element))
            <li class="page-item disabled"><span class="page-link">{{$element}}</span></li>  
        @endif
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator -> currentPage())
                    <li class="page-item active my-active"><span class="page-link">{{$page}}</span></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{$page}}</a></li>    
                    @endif
            @endforeach
        @endif
    @endforeach
    @if ( $paginator -> hasMorePages())
     <li class="page-item"><a class="page-link" href="{{ $paginator -> nextpageUrl()}}" rel="next"><i class="fas fa-forward"></i></a></li>
        
    @else
        
    <li class="page-item disabled"> <span class="page-link"><i class="fas fa-forward"></i></span></li>
    @endif
</ul>
@endif