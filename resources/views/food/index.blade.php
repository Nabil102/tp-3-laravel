@extends('base')

@section('content')
<div class="container">
<form class="mt-4" method="POST" action="{{route('food.store')}}">
  @csrf
    <fieldset>
      <legend  class="mt-4">Ajouter une denrée</legend>
      <div class="form-group">
        <label for="formFile" class="form-label mt-4">Image</label>
        <input class="form-control" type="file" id="formFile" name="image" required >
      </div>
      <div class="form-group">
        <label for="exampleTextarea" class="form-label mt-4">Description</label>
        <textarea class="form-control" id="exampleTextarea" rows="3"  name="description" required></textarea>
      </div>
      <div class="form-group">
        <label for="formDateTime" class="form-label mt-4" > Date et heure </label>
        <input class="form-control" type="datetime-local" value="2018-06-12T19:30" id="startDate" name="dateDajout" required>
      </div>
      <div class="form-group">
        <label for="number" class="form-label mt-4" > Temperature de conservation :</label>
        <input id="generer_meteo" class="form-control mt-4" type="text" name="meteo">
        <button id="generer" class="btn btn-primary mt-4">Générer température</button>
      <button type="submit" class="btn btn-primary mt-4">Envoyer</button>
    </fieldset>
  </form>
</div>
<script>"use strict"

function fetchApi() {
    document.getElementById("generer").addEventListener("click", function(e) {
            e.preventDefault();
        let url = `https://api.openweathermap.org/data/2.5/weather?q=quebec&units=metric&appid=586b679a458bf8b7e1222a04b06ca1a0`
        fetch(url)
        .then((response)=> {
            return response.json();
        })
        .then((data)=> {
            console.log(data.main.temp)
        document.getElementById("generer_meteo").value= data.main.temp;
        })
      });

}
console.log(fetchApi())</script>

@endsection

