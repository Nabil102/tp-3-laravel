
@extends('base')
@section('content')
    <main class="container pt-5">
        @if($message = Session::get('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>{{$message}}</strong> 
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
        @endif
        <section class="mb-5">
            <div class="bg-light p-5 rounded">
                <h1>Faire un don d’aliments</h1>
                <p class="lead">Les Canadiens jettent plus de nourriture qu’ils ne le réalisent – de la nourriture qui aurait pu, à un moment donné, être consommée.</p>
                <p class="lead">Ensemble nous pouvons diminuer notre empreinte écologique et réduire le gasspillage alimentaire au Canada.</p>
                <a class="btn btn-lg btn-primary" href="{{ route('register') }}" role="button">Inscription &raquo;</a>
            </div>
        </section>
        <section>
                <div class="row">
                    @foreach ($foods as $food)
                    <div class="col-sm-4">
                    <div class="card m-3">
                        <div class="card-body">
                            <img src={{$food->image}} class="card-img-top" alt="...">
                            <p class="card-text mt-3"> <strong> Description : </strong> {{$food -> description}}</p>
                            <p class="card-text"><strong> Date de publication : </strong> {{$food -> created_at}}</p>
                            <p class="card-text"><strong> Température de conservation : </strong>{{$food -> meteo}} °C</p>
                            @if (Auth::user())
                            @if(Auth::user() -> role === 'MEMBER')
                            <a href="{{ route('profil.index', Auth::user()->id)}}" class="btn btn-warning">Réserver</a>
                            @endif
                            @else
                            <a href="{{route('login')}}" class="btn btn-warning">Réserver</a>
                            @endif 
                        </div>
                    </div>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center mt-5">
                {{$foods -> links('custom')}}

                </div>

        </section>
    </main>
@endsection