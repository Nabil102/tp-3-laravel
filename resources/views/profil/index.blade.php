@extends('base')

@section('content')
<div>


<div class="container mt-5">
    @if($message = Session::get('success'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>{{$message}}</strong> 
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
  <section>
    <div class="row">
        <div class="col-sm-4">
        <div class="card m-3">
            <div class="card-body">
                <img src="{{ asset('img/userImage.png') }}" class="card-img-top" alt="...">
                <p class="card-text mt-3"> <strong> nom : </strong> {{Auth::user()->name}}</p>
                <p class="card-text"><strong> Courriel : </strong> {{Auth::user()->email}}</p>
                <p class="card-text"><strong> Adress : </strong>{{Auth::user()->address}}</p>
                <p class="card-text"><strong> ville : </strong>{{Auth::user()->city}}</p>
                <a href="{{ route('profil.edit', Auth::user()->id) }}"  ><button type="submit" class="btn btn-primary mt-4"> Modifier </button></a>
                <form action={{ route('profil.delete', Auth::user()->id) }} method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-primary mt-4"> Supprimer </button>
                </form>
                
            
            </div>
        </div>
        </div>
    </div>
    <div class="d-flex justify-content-center mt-5">
    </div>

</section>
</div>
</div>


@endsection