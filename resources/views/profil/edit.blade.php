@extends('base')

@section('content')

<div class="container">
<form class="mt-4" method="POST" action="{{ route('profil.update', Auth::user()->id) }}">
  @method('PUT')
  @csrf
    <fieldset>
      <legend  class="mt-4">Modifier mon Profil</legend>
      <div class="form-group">
        <label for="pseudo" class="form-label mt-4">Nom d'utilsateur</label>
        <input class="form-control" type="text"  name="name" value="{{Auth::user()->name}}" required >
      </div>
      <div class="form-group">
        <label for="email" class="form-label mt-4">Courriel</label>
        <input class="form-control" type="email" name="email" value="{{Auth::user()->email}}" required>
      </div>
      <div class="form-group">
        <label for="formDateTime" class="form-label mt-4" >Adresse</label>
        <input class="form-control" type="text"  name="address" value="{{Auth::user()->address}}" required>
      </div>
      <div class="form-group">
        <label for="city" class="form-label mt-4">City</label>
        <input class="form-control" type="text"  name="city" value="{{Auth::user()->city}}" required >
      </div>
      <div class="form-group">
        <label for="number" class="form-label mt-4" > Mot de passe </label>
        <input class="form-control" type="password" id="pass" name="password" minlength="8" value="{{Auth::user()->password}}" required>
      </div>
      <button type="submit" class="btn btn-primary mt-4">Eregistrer</button>
    </fieldset>
  </form>
</div>

@endsection
