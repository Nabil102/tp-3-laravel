
@extends('base')
@section('content')
    <main class="container pt-5">
        <section class="mb-5">
            <div class="bg-light p-5 rounded">
                <h1>Faire un don d’aliments</h1>
                <p class="lead">Les Canadiens jettent plus de nourriture qu’ils ne le réalisent – de la nourriture qui aurait pu, à un moment donné, être consommée.</p>
                <p class="lead">Ensemble nous pouvons diminuer notre empreinte écologique et réduire le gasspillage alimentaire au Canada.</p>
            </div>
        </section>
        <section>
                <div class="row">
                    @foreach ($foods as $food)
                    <div class="col-sm-4">
                    <div class="card m-3">
                        <div class="card-body">
                            <img src={{$food->image}} class="card-img-top" alt="...">
                            <p class="card-text mt-3"> <strong> Description : </strong> {{$food -> description}}</p>
                            <p class="card-text"><strong> Date de publication : </strong> {{$food -> created_at}}</p>
                            <p class="card-text"><strong> Température de conservation : </strong>{{$food -> meteo}}</p>
                            <a href="#" class="btn btn-warning">Réserver</a>
                        </div>
                    </div>
                    </div>
                    @endforeach
                </div>
                <div class="d-flex justify-content-center mt-5">
                {{$foods -> links('custom')}}

                </div>

        </section>
    </main>
@endsection
