<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Food;
use Illuminate\Http\Request;


class Maincontroller extends Controller
{
    public function home() {
        $foods = Food::paginate(8);
        return view ('home', [
            'foods' => $foods
        ]);
    }    
};
