<?php

use App\Http\Controllers\FoodController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\Maincontroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Maincontroller::class, 'home']) -> name('home');
Route::get('/food/ajouter', [FoodController::class, 'index']) -> name('food.index')-> middleware('member');
Route::get('/member/profil', [ProfilController::class, 'index']) -> name('profil.index')-> middleware('member');
Route::get('/member/profil/{id}', [ProfilController::class, 'edit']) -> name('profil.edit')-> middleware('member');
Route::post('/food/denree/ajouter', [FoodController::class, 'store']) -> name('food.store')-> middleware('member');
Route::put('/member/profil/update/{id}', [ProfilController::class, 'update']) -> name('profil.update')-> middleware('member');
Route::delete('/member/profil/delete/{id}', [ProfilController::class, 'destroy']) -> name('profil.delete')-> middleware('member');

Route::resource('Profil', ProfilController::class);




Auth::routes();


